from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

import time

from django.contrib.auth.models import User

class FunctionalTest(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        options = Options()
        options.headless = True
        cls.selenium = webdriver.Firefox(options=options)
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


    def test_search_book(self):
        self.selenium.get(self.live_server_url + '/search_book/')
        time.sleep(10)
        
        input_form = self.selenium.find_element_by_id('search-input')
        input_form.send_keys('tes')

        self.selenium.find_element_by_id('search-button').click()

        time.sleep(10)
        hasil = self.selenium.find_element_by_id('hasil_list')
        self.assertIn('tes', hasil.get_attribute('innerHTML').lower())

        input_form.clear()
        input_form.send_keys('idolm@ster')
        self.selenium.find_element_by_id('search-button').click()

        time.sleep(10)
        hasil = self.selenium.find_element_by_id('hasil_list')
        self.assertNotIn('tes', hasil.get_attribute('innerHTML').lower())
        self.assertIn('idolm@ster', hasil.get_attribute('innerHTML').lower())

    def test_login(self):
        self.selenium.get(self.live_server_url + '/login/')

        navbar = self.selenium.find_element_by_id('main_navbar')
        self.assertIn('Login', navbar.get_attribute('innerHTML'))

        self.selenium.find_element_by_id('id_username').send_keys('tes_user')
        self.selenium.find_element_by_id('id_password').send_keys('tes_password')
        self.selenium.find_element_by_id('login-button').click()
        time.sleep(3)

        self.assertIn('ID / Password salah atau belum terdaftar', self.selenium.page_source)

        user = User.objects.create_user('tes_user', 'tes@tes.com', 'tes_password')
        user.first_name = 'Kak'
        user.last_name = 'Pewe'
        user.save()

        self.selenium.find_element_by_id('id_username').clear()
        self.selenium.find_element_by_id('id_password').clear()
        self.selenium.find_element_by_id('id_username').send_keys('tes_user')
        self.selenium.find_element_by_id('id_password').send_keys('tes_password')

        self.selenium.find_element_by_id('login-button').click()
        time.sleep(2)

        navbar = self.selenium.find_element_by_id('main_navbar')
        self.assertNotIn('Login', navbar.get_attribute('innerHTML'))
        self.assertIn('Logout', navbar.get_attribute('innerHTML'))

        logout_button = self.selenium.find_element_by_id('logout-button')
        logout_button.click()
        time.sleep(2)

        navbar = self.selenium.find_element_by_id('main_navbar')
        self.assertIn('Login', navbar.get_attribute('innerHTML'))
        self.assertNotIn('Selamat datang, Kak Pewe', self.selenium.page_source)
        self.assertNotIn('Logout', navbar.get_attribute('innerHTML'))

