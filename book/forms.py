from django import forms

class SearchForm(forms.Form):
    attrs = {
        'type':'text',
        'class': 'form-control',
        'id':'search-input'
    }

    text_input = forms.CharField(label='Search Your Book Here!', required=True, widget=forms.TextInput(attrs=attrs))
